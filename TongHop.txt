- iscsi : nghĩ đến storage gate way
- Trong Storage Gateway:
 + Volume Gate Way : độ trễ thấp nhất
 + Tape Gateway: backupdata lâu dài, có độ trễ cao hơn
***học lại Route 53, System Manager,inter-face endpoint vs gateway endpoint
- Active Directory để quản lý access vào file-system trên on-premis, muốn dùng Active Directory kiểm soát access cả FSx nữa thì cần join file system
- Bài toán muốn cache nhiều phiên bản của content và trả về phiên bản tương ứng cho các device
 -> Dùng cloundfront cache nhiều version
 -> Lambda@edge để lấy thông tin user-agent và trả về content tương ứng
- Amazon Aurora global database, muốn nhanh thì dùng warm standby, pilot light chậm hơn
- Để lambda access được vào các service khác thì phải dùng IAM role
- Giảm phí truyền tải data cho các user ở region khác tải file xuống -> CLoud front
- Không muốn chỉ định instance type thì chỉ có serverless
- Shield dùng để chống DDos
- Macie quét thông tin nhạy cảm trên S3
- System Manager Run Command 
 + cho phép chạy và thực thi những comment từ xa đối vs EC2,server on-premise được quản lý bởi System Manager
-  complex SQL queries -> nghĩ đến AWS Glue( serverless)
- Muốn access vào EC2 mà ko cần SSH key -> System Manager
- Xử lý bất đồng bộ -> SQS
- Bài toán EC2 truy cập đến 1 số Url nhất định, nguồn truy cập từ internet bị block -> AWS Network Firewall
- EBS fast snapshot là tính năng để restore 1 cách nhanh nhất
- Lake Formation
 + Quản lý data lake
 + Quản lý về access (permission) (có tag-based access control)
- Trong Auto Scaling có predictive có thể dự đoán trend sắp tới thế nào
- Xử lý bài toán muốn application xử lý những massage trong SQS có dung lượng > 256GB -> SQS Extened Client Library
- Active Directory để authen những quyền chia sẻ file (window),Fsx File for window
- RestFull API thì phải là interface-enpoint,gateway enpoint cho DynamoDB vs S3 thôi
- Muốn mã hóa in-transit: SQS thì dùng queue policy,SNS dùng topic policy để deny những access không TLS
- Sử dụng api usage plans và api keys để hạn chế truy cập của user thường, cho phép user vip truy cập
- Muốn access vào Lake Formation thì có access control comlumn-lever security
- Muốn gửi report khi EC2 terminate or tạo ra-> lifecycle hook
- Muốn lambda scaling -> provisioned concorrency
- GLue có job bookmark để đánh dấu data nào đã xử lý rồi, nên lần sau chỉ xử lý data mới thôi
- Dynamo DB có point-in-time có thể cho 1 lịch để back up
- Để transfer data giữa SaaS apllication và AWS service thì có AppFlow
- Trong 1 Organizations thì có 1 account không dùng hết Saving Plan thì có thể bật discount sharing để các tài khoản con có thể sử dụng được
- DynamoDB có Time to live để xóa những data sao bao nhiêu ngày (S3 có lifecycle policy)
- S3 mutilpart upload
- AWS Network Firewall là tường lửa, đầu ra của EC2
- Autoscaling có predictive scaling policy để scale theo dự đoán trend từ data cũ
- sử dụng api usage plans và api keys để hạn chế truy cập của user thường
- EC2 Instance store 
 + tắt Ec2 thì sẽ mất data
 + performance cao
 + Dùng trong use case chạy batch
- Spot Fleet : kết hợp Ondemand + Spot
- Các service AI
 * Reko
 + Nhận diện đối tượng có trong ảnh
 + Nhận diện quản lý khuôn mặt
* Poly
 + Convert text -> âm thanh
* Lex & Connect
 + Buld con bot để nghe hiểu , trung tâm cuộc gọi có những con bot ảo trả lời
* Comprehend 
 - Xử lý ngôn ngữ tự nhiên
* Sage Marker
 - Build và training ML
* Forecast
 - Đưa ra dự đoán   
* Kendra
 - Tìm kiếm tài liệu
* Personalize
 - Cá nhân hóa, đưa ra thông tin những cái mà mình chú ý (giống facebook, nghe thông tin để quảng cáo)
* Textract
 - Export content từ ảnh
- Lưu trữ ở S3 Standard phải 30 ngày xong mới chuyển được sang cái khác
- Kinesis data stream không thể chuyển dữ liệu trực tiếp đến S3
- Cloudfront không chỏ được DNS record Route53
- S3 chỉ cho phép kích hoạch SQS không cho phép SQS Fifo
- Kinesi Data Fire House ko lưu đến DynamoDB được

